What's this?
=============
Ａ simple practice following [Advanced Android in Kotlin 02.3:Clipping Canvas Objects] in CodeLabs.
<br>If you have no idea how to clip a canvas, I suggest you take this course.  

Friendly reminder
=============
There are 8 fun named drawXXXExample and they correspond to 8 areas in UI.
<br>There are many interesting knowledge in "Learn more" part. 

[Advanced Android in Kotlin 02.3:Clipping Canvas Objects]:https://developer.android.com/codelabs/advanced-android-kotlin-training-clipping-canvas-objects#0

